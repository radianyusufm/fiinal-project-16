@extends('master.master')


@section('content')

<div class="main-content">
	<div class="container-fluid">
		<div class="panel panel-profile">
			<div class="clearfix">

				<!-- LEFT COLUMN -->
				<div class="profile-left">
					<!-- PROFILE HEADER -->
					<div class="profile-header">

						<div class="profile-main">
							<img src="assets/img/user-medium.png" class="img-circle" style="margin: 20px">
							<h2 class="name">Samuel Gold</h2>
						</div>

						<div class="profile-stat">
							<div class="row">
								<div class="col-md-6 stat-item">
									45 <span>Follow</span>
								</div>

								<div class="col-md-6 stat-item">
									2174 <span>Followers</span>
								</div>
							</div>
						</div>
					</div>
					<!-- END PROFILE HEADER -->
					<!-- PROFILE DETAIL -->
					<div class="profile-detail">
						<div class="profile-info">
							<h4 class="heading">Basic Info</h4>
							<ul class="list-unstyled list-justify">
								<li>Nama <span>Samuel Gold</span></li>
								<li>Email <span>samuel</span></li>
								<li>Jenis Kelamin <span>Laki-laki</span></li>
								<li>Tanggal Lahir <span>24 Aug, 2016</span></li>
							</ul>
						</div>

						{{-- modal form edit profil --}}
						<div class="text-center"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Edit Profile</button></div>
					</div>
					<!-- END PROFILE DETAIL -->
				</div>
				<!-- END LEFT COLUMN -->

				<div class="profile-right">
					<h4 class="heading">Postingan</h4>


					<ul>
						<li>
							<span><b>Judul Postingan ini </b></span> <br>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
							ut aliquip ex ea commodo consequat. Duis aute irure dolor .
							<a href="/home/1/detail">Detail</a>
						</li>
					</ul>


					<br>
					<br>
					<br>



					<ul>
						<li>
							<span><b>Judul Postingan ini </b></span> <br>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
							ut aliquip ex ea commodo consequat. Duis aute irure dolor .
							<a href="/home/2/detail">Detail</a>
						</li>
					</ul>


					<br>
					<br>
					<br>


					<ul>
						<li>
							<span><b>Judul Postingan ini </b></span> <br>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
							ut aliquip ex ea commodo consequat. Duis aute irure dolor .
							<a href="/home/3/detail">Detail</a>
						</li>
					</ul>


					<br>
					<br>
					<br>

				</div>

			</div>
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Edit Profil</h4>
			</div>
			<div class="modal-body">





				<form>
					<div class="form-group">
						<label for="exampleInputFile">Foro Profil</label>
						<input type="file" id="exampleInputFile">
					</div>
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Nama</label>
						<input type="text" class="form-control" placeholder="Nama Lengkap">
					</div>

					<div class="form-group">
						<label for="exampleInputPassword1">Gender</label>
						<select>
							<option>Laki-laki</option>
							<option>Perempuan</option>
						</select>
					</div>



					<div class="form-group">

						<label for="exampleInputPassword1">Tanggal Lahir</label>

						<input type="date" class="datepicker">
					</div>


					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary">Update Profile</button>
					</div>
				</form>



			</div>

		</div>
	</div>
</div>
@endsection
