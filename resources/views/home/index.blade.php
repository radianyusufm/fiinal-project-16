  @extends('master.master')
  @section('content')


  <div class="main-content">
      <div class="container-fluid">
          <div class="row">
              <div class="col-sm-8">
                  <div class="panel panel-headline demo-icons">
                      <div class="panel-heading">
                          <div class="row">
                              <div class="col-sm-2">
                                  <a href="/profil"><img src="assets/img/user-medium.png" class="img-circle" alt="Avatar"></a>
                              </div>
                              <div class="col-sm-10">
                                  <a href="/profil">
                                      <h3 class="panel-title">Nama User</h3>
                                      <span>tanggal update</span>
                                  </a>
                              </div>
                          </div>
                      </div>
                      <div class="panel-body">

                          <h4>Judul Postingan</h4>
                          <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                              sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                              ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                          </p>
                          {{-- jika user posting gambar maka tampilkan --}}
                          <img src="assets/img/profile-bg.png" class="img-responsive" alt="Responsive image">
                          <hr>
                          <span class="glyphicon glyphicon-hand-right"></span>

                          <span class="glyphicon glyphicon-thumbs-down"></span>
                      </div>
                  </div>

              </div>
          </div>


          <div class="row">
              <div class="col-sm-8">
                  <div class="panel panel-headline demo-icons">
                      <div class="panel-heading">
                          <div class="row">
                              <div class="col-sm-2">
                                  <a href="/profil"><img src="assets/img/user4.png" class="img-circle" alt="Avatar"></a>
                              </div>
                              <div class="col-sm-10">
                                  <a href="/profil">
                                      <h3 class="panel-title">Nama User</h3>
                                      <span>tanggal update</span>
                                  </a>
                              </div>
                          </div>
                      </div>
                      <div class="panel-body">



                          <h4>Judul Postingan</h4>
                          <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                              sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                              ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                          </p>

                      </div>
                  </div>

              </div>
          </div>
      </div>
  </div>


  @endsection
