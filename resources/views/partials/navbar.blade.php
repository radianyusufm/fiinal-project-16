
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="brand"  >
    <a href="/home" ><img src="assets/img/ok.jpeg" class="img-responsive logo" width='70px'></a>
  </div>
  <div class="container-fluid">
    <div class="navbar-btn">
      <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
    </div>
    <form class="navbar-form navbar-right">
      <div class="input-group">
        <input type="text" value="" class="form-control" placeholder="Cari orang...">
        <span class="input-group-btn"><button type="button" class="btn btn-primary">Go</button></span>
      </div>
    </form>
  </div>
</nav>
