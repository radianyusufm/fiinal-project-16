<div id="sidebar-nav" class="sidebar">
  <div class="sidebar-scroll">
    <nav>
      <ul class="nav">
        <li><a href="/home" class=""><i class="glyphicon glyphicon-home"></i> <span>Home</span></a></li>
        <li><a href="/profil" class=""><i class="glyphicon glyphicon-user"></i> <span>Profil</span></a></li>
        <li><a href="/posting" class=""><i class="glyphicon glyphicon-pencil"></i> <span>Posting</span></a></li>
        <li><a href="#" class=""><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a></li>

      </ul>
    </nav>
  </div>
</div>
